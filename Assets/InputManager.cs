﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public Game game;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            game.Begin();
        
        if (Input.GetKeyDown(KeyCode.W))
            game.MoveRakette(Player.One, Directions.Up);
        if(Input.GetKeyDown(KeyCode.S))
            game.MoveRakette(Player.One, Directions.Down);
        if(Input.GetKeyUp(KeyCode.W))
            game.StopRakette(Player.One, Directions.Up);
        if(Input.GetKeyUp(KeyCode.S))
            game.StopRakette(Player.One, Directions.Down);


        if(Input.GetKeyDown(KeyCode.UpArrow))
            game.MoveRakette(Player.Two, Directions.Up);
        if(Input.GetKeyDown(KeyCode.DownArrow))
            game.MoveRakette(Player.Two, Directions.Down);
        if(Input.GetKeyUp(KeyCode.UpArrow))
            game.StopRakette(Player.Two, Directions.Up);
        if(Input.GetKeyUp(KeyCode.DownArrow))
            game.StopRakette(Player.Two, Directions.Down);
    }
}
