﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Serialization;

public class Rakete : MonoBehaviour
{
    public float spin;
    [FormerlySerializedAs("velocity")] 
    public float speed;

    private int direction = 0;
    public Vector3 velocity;
    private float rotationAngle = 0;
    public float maxRotationAngle = 25;

    void Start()
    {
        
    }
    
    void Update()
    {
        
        Debug.DrawRay(transform.position, velocity*10, Color.red);
    }

    private void FixedUpdate()
    {
        rotationAngle += ((maxRotationAngle * direction) - rotationAngle) * 0.02f;
        transform.rotation = Quaternion.Euler(0,0,rotationAngle);
        transform.position += velocity;
    }

    public void Reset()
    {
        throw new System.NotImplementedException();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
    }

    public void BeginMovement(Directions directions)
    {
        direction += (directions == Directions.Up ? 1 : -1);
        velocity = speed * direction * Vector3.up;
    }

    public void StopMovement(Directions directions)
    {
        direction -= directions == Directions.Up ? 1 : -1;
        velocity = speed * direction * Vector3.up;
        rotationAngle = 0;
    }
}
