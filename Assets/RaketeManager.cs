﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raquette : MonoBehaviour
{
    public float Speed;
    public int Direction;
    public float MaxAngleRotation;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Time.deltaTime * Speed * Direction * Vector3.up;
        var maxRotation = new Vector3(1,1,0);

        //var currentAngle = Mathf.Lerp(transform.rotation.z, MaxAngleRotation * Direction, Time.deltaTime * 8);
        //transform.rotation = Quaternion.Euler(new Vector3(0, 0, currentAngle));
    }
}
