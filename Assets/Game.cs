﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class Game : MonoBehaviour
{
    public Rakete playerOne;
    public Rakete playerTwo;
    public BallManager ball;

    public Dictionary<Player, Rakete> players = new Dictionary<Player, Rakete>();
    // Start is called before the first frame update
    void Start()
    {
        players.Add(Player.One, playerOne);
        players.Add(Player.Two, playerTwo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveRakette(Player player, Directions direction)
    {
        players[player].BeginMovement(direction);
    }

    public void Begin()
    {
        ball.Reset();
        playerOne.Reset();
        playerTwo.Reset();
    }

    public void StopRakette(Player player, Directions direction)
    {
        players[player].StopMovement(direction);
    }
}
