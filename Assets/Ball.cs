﻿using UnityEngine;

public class Ball : MonoBehaviour
{

    public float Speed;
    public float Increment;
    public int MaxForce;
    public float AirFriction;
    public float SpinMagnitude;
    private Vector3 Force;
    private Vector3 Spin;
    
    // Start is called before the first frame update
    void Start() => Reset();

    // Update is called once per frame
    void Update()
    {
        
        transform.position += Time.deltaTime * (Force + Spin);
        //Debug.DrawLine(transform.position, transform.position + Force, Color.white, .5f);
        //Debug.DrawRay(transform.position, Force.normalized, Color.cyan); 
        //Debug.DrawRay(transform.position, Spin.normalized, Color.blue);
        //Debug.DrawRay(transform.position, (Force + Spin).normalized, Color.red);
        Spin *= AirFriction;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        var normal = other.contacts[0].normal;
        Force = Vector3.Reflect(Force, normal);
        
        
        if (other.gameObject.GetComponent<Raquette>() != null)
            RaquetteCollision(other.gameObject.GetComponent<Raquette>().Direction);
        else
            WallCollision(other.gameObject, normal);
    }

    private void WallCollision(GameObject otherGameObject, Vector2 normal)
    {
        Spin = Vector3.Reflect(Spin, normal);
        if (otherGameObject.CompareTag("Side"))
            Reset();
    }

    private void RaquetteCollision(int direction)
    {
        Force.y += Spin.y;
        Spin += direction * SpinMagnitude * Vector3.up;

        if(Force.magnitude < MaxForce) 
            Force.y *= Increment;
    }

    public void Reset()
    {
        Spin = new Vector3(0,0,0);
        Force = new Vector3(2, 0.5f, 0).normalized * Speed;
        transform.position = Vector3.zero;
    }
}
