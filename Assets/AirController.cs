﻿using System;
using System.Collections;
using System.Collections.Generic;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class AirController : MonoBehaviour
{
    public Raquette playerOne;
    public Raquette playerTwo;
    public Ball ball;
    
    void Start()
    {
        //AirConsole.instance.SetActivePlayers (2);
        //AirConsole.instance.onMessage += OnMessage;
        //AirConsole.instance.onConnect += OnConnect;
        //AirConsole.instance.onDisconnect += OnDisconnect;
    }

    void GameStart()
    {
        ball.Speed = 8;
        ball.Reset();
    }

    void GameStop()
    {
        ball.Speed = 0;
        ball.Reset();
    }
    
    void OnConnect (int device_id)
    {
        if (AirConsole.instance.GetActivePlayerDeviceIds.Count != 0) 
            return;
        if (AirConsole.instance.GetControllerDeviceIds().Count >= 2)
            GameStart();
    }

    void OnDisconnect (int device_id) {
        int activePlayer = AirConsole.instance.ConvertDeviceIdToPlayerNumber (device_id);
        if (activePlayer != -1) {
            if (AirConsole.instance.GetControllerDeviceIds().Count >= 2)
                GameStart();
            else
            {
                AirConsole.instance.SetActivePlayers (0);
                GameStop();
            }
        }
    }

    void OnMessage (int device_id, JToken data) {
        int active_player = AirConsole.instance.ConvertDeviceIdToPlayerNumber(device_id);
        if (active_player != -1)
        {
            if (active_player == 0)
                playerOne.Direction = (int) data["move"];
            if (active_player == 1)
                playerTwo.Direction = (int) data["move"];
        }
    }
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
            GameStart();
        if(Input.GetKeyDown(KeyCode.W))
            playerOne.Direction += 1;
        if(Input.GetKeyDown(KeyCode.S))
            playerOne.Direction -= 1;
        if(Input.GetKeyUp(KeyCode.W))
            playerOne.Direction -= 1;
        if(Input.GetKeyUp(KeyCode.S))
            playerOne.Direction += 1;
        
        if(Input.GetKeyDown(KeyCode.UpArrow))
            playerTwo.Direction += 1;
        if(Input.GetKeyDown(KeyCode.DownArrow))
            playerTwo.Direction -= 1;
        if(Input.GetKeyUp(KeyCode.UpArrow))
            playerTwo.Direction -= 1;
        if(Input.GetKeyUp(KeyCode.DownArrow))
            playerTwo.Direction += 1;
    }
}
