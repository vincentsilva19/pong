﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

public class BallManager : MonoBehaviour
{

    public Vector3 velocity;
    [FormerlySerializedAs("spin")] 
    public Vector3 angularVelocity = Vector3.zero;
    public float initialSpeed;
    public float airFriction = 0.1f;
    public float maxVelocity;
    public int maxSpin;
    void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        //Debug.DrawRay(transform.position, velocity, Color.red);
        Debug.DrawRay(transform.position, angularVelocity*2, Color.yellow);
        transform.position += (Time.deltaTime * velocity);
    }

    void FixedUpdate()
    {
        velocity = Quaternion.Euler(0, 0, angularVelocity.magnitude) * velocity;
        angularVelocity *= airFriction;
    }

    public void Reset()
    {
        velocity = Vector3.right * initialSpeed;
        transform.position = Vector3.zero;
        angularVelocity = Vector3.zero;
    }
    

    private void OnCollisionEnter2D(Collision2D other)
    {
        var normal = other.contacts[0].normal;
        velocity = Vector3.Reflect(velocity, normal);
        angularVelocity = Vector3.Reflect(angularVelocity, normal);
        
        if (other.gameObject.CompareTag("Player"))
            RaquetteCollision(other.gameObject.GetComponent<Rakete>(), normal);
        if (other.gameObject.CompareTag("Side"))
            WallCollision();
    }

    private void WallCollision()
    {
        Reset();
    }

    private void RaquetteCollision(Rakete target, Vector2 normal)
    {
        angularVelocity.y = target.velocity.y * 3;
        Debug.Log(velocity.magnitude);
        Debug.Log(maxVelocity);
        if (velocity.magnitude < maxVelocity)
            velocity *= 1.2f;
    }
}
